## JS-14 (Typescript-2) - Typescript example project
This is an example Typescript project covering multiple Typescript features. Based on UDEMY course: https://www.udemy.com/understanding-typescript.

#### Used commands and libs:
* npm init
* npm install lite-server --save-dev
* tsc <filename.ts>
* tsc --init
* tsc
