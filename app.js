"use strict";
console.log("LET & CONST");
// let & const
var variable = "Test";
console.log(variable);
//variable = 25;
variable = "Another variable";
console.log(variable);
var maxLevels = 100;
console.log(maxLevels);
//maxLevels = 99;
// Block scope
console.log("BLOCK SCOPE");
function reset() {
    //console.log(variable);
    var variable = null;
    console.log(variable);
}
reset();
console.log(variable);
// Arrow functions
console.log("ARROW FUNCTIONS");
var addNumbers = function (number1, number2) {
    return number1 + number2;
};
console.log(addNumbers(5, 5));
var multiplyNumbers = function (number1, number2) { return number1 * number2; };
console.log(multiplyNumbers(3, 10));
var greet = function () { return console.log("Hello"); };
var greet2 = function () { return console.log("Hello"); };
var greet3 = function (friendName) { return console.log("Hello " + friendName); };
greet();
greet2();
greet3("Max");
// Arrow functions - default parameters
console.log("ARROW FUNCTIONS - DEFAULT PARAMETERS");
var countDown = function (start, end) {
    if (start === void 0) { start = 10; }
    if (end === void 0) { end = start - 5; }
    console.log("Start!", start);
    console.log("Start (end value)!", end);
    while (start > 0) {
        start--;
    }
    console.log("Done!", start);
};
countDown(20);
countDown();
// Rest & Spread operator
console.log("REST AND SPREAD OPERATORS");
var numbers = [1, 10, 99, -25];
console.log(Math.max(33, 77, 10, -3));
//console.log(Math.max(numbers));
console.log(Math.max.apply(Math, numbers)); // SPREAD OPERATOR
function makeArray(arg1, arg2) {
    return [arg1, arg2];
}
console.log(makeArray(1, 2));
function makeArray2(name) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    console.log(name);
    return args;
}
console.log(makeArray2("Max", 1, 2, 3, 4, 5, 6, 7, 8, 9));
// Destructuring arrays and objects
console.log("DESTRUCTURING");
var myHobbies = ["Cooking", "Sports"];
var hobby1 = myHobbies[0];
var hobby2 = myHobbies[1];
var hobby3 = myHobbies[0], hobby4 = myHobbies[1];
console.log(myHobbies[0], myHobbies[1]);
console.log(hobby1, hobby2);
console.log(hobby3, hobby4);
var userData = { userName: "Max", age: 27 };
var myName = userData.userName, age = userData.age;
console.log(myName, age);
// Template literals
console.log("TEMPLATE LITERALS");
var userName = "max";
//const greeting = "Hello, Im " + userName;
var greeting = "This is heading!\nIm " + userName + "\nThis is cool.";
console.log(greeting);
// Symbols, iterators, generators, ...
