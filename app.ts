console.log("LET & CONST");

// let & const
let variable = "Test";
console.log(variable);

//variable = 25;
variable = "Another variable";
console.log(variable);

const maxLevels = 100;
console.log(maxLevels);
//maxLevels = 99;


// Block scope
console.log("BLOCK SCOPE");

function reset() {
    //console.log(variable);
    let variable = null;
    console.log(variable);
}

reset();
console.log(variable);


// Arrow functions
console.log("ARROW FUNCTIONS");

const addNumbers = function (number1: number, number2: number): number {
    return number1 + number2;
};
console.log(addNumbers(5, 5));

const multiplyNumbers = (number1: number, number2: number): number => number1 * number2;
console.log(multiplyNumbers(3, 10));

const greet = () => console.log("Hello");
const greet2 = (): void => console.log("Hello");
const greet3 = friendName => console.log("Hello " + friendName);
greet();
greet2();
greet3("Max");


// Arrow functions - default parameters
console.log("ARROW FUNCTIONS - DEFAULT PARAMETERS");
const countDown = (start: number = 10, end: number = start - 5): void => {
    console.log("Start!", start);
    console.log("Start (end value)!", end);

    while ( start > 0 ) {
        start--;
    }

    console.log("Done!", start);
};

countDown(20);
countDown();


// Rest & Spread operator
console.log("REST AND SPREAD OPERATORS");

const numbers = [1, 10, 99, -25];
console.log(Math.max(33, 77, 10, -3));
//console.log(Math.max(numbers));
console.log(Math.max(...numbers));  // SPREAD OPERATOR

function makeArray(arg1: number, arg2: number) {
    return [arg1, arg2];
}

console.log(makeArray(1, 2));

function makeArray2(name: string, ...args: number[]) {  // REST OPERATOR
    console.log(name);
    return args;
}

console.log(makeArray2("Max", 1, 2, 3, 4, 5, 6, 7, 8, 9));


// Destructuring arrays and objects
console.log("DESTRUCTURING");
const myHobbies = ["Cooking", "Sports"];
const hobby1 = myHobbies[0];
const hobby2 = myHobbies[1];
const [hobby3, hobby4] = myHobbies;

console.log(myHobbies[0], myHobbies[1]);
console.log(hobby1, hobby2);
console.log(hobby3, hobby4);

const userData = {userName: "Max", age: 27};
const {userName: myName, age} = userData;
console.log(myName, age);


// Template literals
console.log("TEMPLATE LITERALS");
const userName = "max";
//const greeting = "Hello, Im " + userName;
const greeting = `This is heading!
Im ${userName}
This is cool.`;

console.log(greeting);

// Symbols, iterators, generators, ...
